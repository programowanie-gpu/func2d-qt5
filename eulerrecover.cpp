// see https://stackoverflow.com/questions/6563810/m-pi-works-with-math-h-but-not-with-cmath-in-visual-studio
#define _USE_MATH_DEFINES

#include <QMatrix4x4>

#include <cmath>

// debug
#include <iostream>
#include <iomanip>


#include "eulerrecover.h"

void qNormalizeAngle(int &angle)
{
    while (angle < 0)
        angle += 360;
    while (angle > 360)
        angle -= 360;
}

float qNormalizedAngle(float angle)
{
    while (angle < 0)
        angle += 360;
    while (angle > 360)
        angle -= 360;

    return angle;
}

// angle in degrees
static float angle_dist(float a, float b)
{
    a = qNormalizedAngle(a);
    b = qNormalizedAngle(b);
    return fminf(fabsf(a - b), 360 - fabsf(a - b));
}

static float rotation_dist(float xAngle, float yAngle, float zAngle,
                            float xGot,   float yGot,   float zGot)
{
    return angle_dist(xAngle, xGot) +
           angle_dist(yAngle, yGot) +
           angle_dist(zAngle, zGot);
}

static float norm4x4_1(const QMatrix4x4 &mat)
{
    float norm = 0.0;
    const float *p = mat.constData();

    for (int i = 0; i < 4*4; ++i) {
        norm += fabsf(p[i]);
    }

    return norm;
}

void eulerAngles(const QMatrix4x4 &mMatrix, // model matrix
                 float &xAngle, float &yAngle, float &zAngle)
{
    float xGot[2],  yGot[2],  zGot[2];  // radians
    float xGotN[2], yGotN[2], zGotN[2]; // degrees, normalized 0..360
    QMatrix4x4 mGot[2], diff[2];
    float dist[2];
    float ang_dist[2];


    // Tait-Bryan angles (nautical angles, or Cardan angles)
    // z-y'-x'' - yaw, pitch, roll  or  heading, elevation, bank
    // rotation angle in degrees (0..360 or -180..180)

    yGot[0] = asinf(fmaxf(-1.0,fminf(1.0,mMatrix(0,2))));
    if (yGot[0] >= 0.0f) {
        yGot[1] =   M_PI - yGot[0];
    } else {
        yGot[1] =   M_PI - yGot[0];
        yGot[0] = 2*M_PI + yGot[0];
    }

    xGot[0] = atan2f(-mMatrix(1,2), mMatrix(2,2));
    xGot[1] = atan2f( mMatrix(1,2),-mMatrix(2,2));

    zGot[0] = atan2f(-mMatrix(0,1), mMatrix(0,0));
    zGot[1] = atan2f( mMatrix(0,1),-mMatrix(0,0));


    // Tait-Bryan angles (nautical angles, or Cardan angles)
    // z-y'-x'' - yaw, pitch, roll  or  heading, elevation, bank
    // rotation angle in degrees (0..360 or -180..180)
    for (int i = 0; i <= 1; i++) {
        xGotN[i] = qNormalizedAngle(180.0/M_PI*xGot[i]);
        yGotN[i] = qNormalizedAngle(180.0/M_PI*yGot[i]);
        zGotN[i] = qNormalizedAngle(180.0/M_PI*zGot[i]);

        mGot[i].rotate(xGotN[i], 1, 0, 0);
        mGot[i].rotate(yGotN[i], 0, 1, 0);
        mGot[i].rotate(zGotN[i], 0, 0, 1);

        diff[i] = mMatrix - mGot[i];
        dist[i] = norm4x4_1(diff[i]);
        ang_dist[i] = rotation_dist(xAngle,   yAngle,   zAngle,
                                    xGotN[i], yGotN[i], zGotN[i]);
    }

#if 0
    std::cout << std::fixed << std::showpos;
    std::cout << "Results: " << std::endl
        <<  " original = ("
        << "x=" << std::setw(11) << xAngle << ", "
        << "y=" << std::setw(11) << yAngle << ", "
        << "z=" << std::setw(11) << zAngle << ")"
        << std::endl
        <<  " prop. 1  = ("
        << "x=" << std::setw(11) << qNormalizedAngle(180.0/M_PI*xGot[0]) << ", "
        << "y=" << std::setw(11) << qNormalizedAngle(180.0/M_PI*yGot[0]) << ", "
        << "z=" << std::setw(11) << qNormalizedAngle(180.0/M_PI*zGot[0]) << ") "
        << (ang_dist[0] < ang_dist[1] ? "=" : " ")
        << (dist[0] < dist[1] ? "x" : " ")
        << std::scientific << " diff=" << dist[0] << std::fixed
        << std::endl
        <<  " prop. 2  = ("
        << "x=" << std::setw(11) << qNormalizedAngle(180.0/M_PI*xGot[1]) << ", "
        << "y=" << std::setw(11) << qNormalizedAngle(180.0/M_PI*yGot[1]) << ", "
        << "z=" << std::setw(11) << qNormalizedAngle(180.0/M_PI*zGot[1]) << ") "
        << (ang_dist[1] < ang_dist[0] ? "=" : " ")
        << (dist[1] < dist[0] ? "x" : " ")
        << std::scientific << " diff=" << dist[1] << std::fixed
        << std::endl;
    std::cout << std::noshowpos;
    std::cout.unsetf(std::ios_base::floatfield);
#endif
}

