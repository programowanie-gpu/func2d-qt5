#ifndef PLOTGLWIDGET_H
#define PLOTGLWIDGET_H

#include <cmath>

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QMatrix4x4>

#include "surfaceplotgl.h"


class PlotGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    explicit PlotGLWidget(QWidget *parent = nullptr);
    ~PlotGLWidget();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    bool animate;

    const SurfacePlotGL &plot() const { return plot3d; }

protected:
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    void draw();
    void cleanup();

    int xRot, yRot, zRot;
    float xAngle, yAngle, zAngle;
    float alpha, beta; // TODO: better name (camera rotation angles)
    float cameraDistance;

    QMatrix4x4 pMatrix;

    SurfacePlotGL plot3d{-1.0f, 1.0f, -1.0f, 1.0f,
                         75, 75,
                         [](float x, float y) -> float {
                            //return expf(-5.0f*(x*x + y*y));
                            float d = hypotf(x, y - 0.3f) * 4.0f;
                            return (1 - d*d) * expf((d * d + -8.0f*x*y - x) / -2.0f);
                         }};

    QOpenGLShaderProgram coloringShaderProgram;
    int numSpotlightVertices;
    QOpenGLBuffer spotlightBuffer;
    float lightAngle;

    float mouseRotationStep;
    QPoint lastMousePosition;

signals:
    // signaling rotation from mouse movement
    void xRotationChanged(int angle);
    void yRotationChanged(int angle);
    void zRotationChanged(int angle);
    // signaling camera distance change from mouse wheel scroll
    void cameraDistanceChanged(float cameraDistance);
    
public slots:
    // slots for xyz-rotation slider
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);
    // slots for checkboxes
    void checkDrawSurface(int status);
    void checkDrawLines(int status);
    void checkDrawPoints(int status);
    void checkDrawCube(int status);

private slots:
    void timeout();
};

#endif // PLOTGLWIDGET_H
