#include "funcxygl.h"

void evaluate(const FuncXYEval &func, QOpenGLBuffer &vbo)
{
    // TODO: remove printing debugging info when moving to animated plot of f(x,y;t)
    qDebug("%s -- for FuncXYEval, without storage", __PRETTY_FUNCTION__);
    if (!vbo.isCreated())
        vbo.create();

    vbo.bind();
    float *values = static_cast<float *>(vbo.map(QOpenGLBuffer::WriteOnly));
    func.evaluate(values);
    vbo.unmap();
    vbo.release();
}

void evaluate(FuncXY &func, QOpenGLBuffer &vbo)
{
    // TODO: remove printing debugging info when moving to animated plot of f(x,y;t)
    qDebug("%s -- for FuncXY, with storage", __PRETTY_FUNCTION__);
    func.evaluate();

    if (!vbo.isCreated())
        vbo.create();

    vbo.bind();
    vbo.write(0, func.data(), static_cast<int>(func.size()*1*sizeof(GLfloat)));
    vbo.release();
}



