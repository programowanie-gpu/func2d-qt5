#-------------------------------------------------
#
# Project created by QtCreator 2015-06-19T12:49:04
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = plot3d_anim_qt5
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32:LIBS += -lopengl32

SOURCES += \
    funcxygl.cpp \
    main.cpp \
    window.cpp \
    funcxy.cpp \
    grid2d.cpp \
    mesh2dgl.cpp \
    openglutils.cpp \
    surfaceplotgl.cpp \
    plotglwidget.cpp \
    eulerrecover.cpp

HEADERS  += \
    funcxygl.h \
    window.h \
    funcxy.h \
    grid2d.h \
    mesh2dgl.h \
    openglutils.h \
    plotglwidget.h \
    surfaceplotgl.h \
    eulerrecover.h

OTHER_FILES += \
    README.md

RESOURCES += \
    resources.qrc

DISTFILES += \
    lightingFragmentShader.fsh \
    lightingVertexShader.vsh \
    coloringVertexShader.vsh \
    coloringFragmentShader.fsh \
    plotFragmentShader.fsh \
    plotVertexShader.vsh
