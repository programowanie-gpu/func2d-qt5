#include "funcxy.h"

#include <cmath>
#include <algorithm>

// debug
#include <iostream>
#include <iomanip>


FuncXYEval::FuncXYEval()
    : m_grid(-8.0f, +8.0f,
             -8.0f, +8.0f,
             25, 25),
      m_func([](float x, float y) {
        float r = hypotf(x, y);
        return r == 0.0f ? 0.0f : sinf(r)/r;
})
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

FuncXYEval::FuncXYEval(Point2D origin,
               float scalex, float scaley,
               size_t Nx, size_t Ny,
               func2d_t func)
    : m_grid(origin, scalex, scaley, Nx, Ny),
      m_func(func)
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

FuncXYEval::FuncXYEval(float xmin, float xmax,
               float ymin, float ymax,
               size_t Nx, size_t Ny,
               func2d_t func)
    : m_grid(xmin, xmax, ymin, ymax, Nx, Ny),
      m_func(func)
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    std::cout << "  size: " << Nx << " x " << Ny << " = " << Nx*Ny << std::endl;
    std::cout << "  range: ["
              << xmin << "," << xmax << "] x ["
              << ymin << "," << ymax << "]" << std::endl;
}

float FuncXYEval::zmin(const std::vector<float> &values) const
{
    return *std::min_element(values.begin(), values.end());
}

float FuncXYEval::zmax(const std::vector<float> &values) const
{
    return *std::max_element(values.begin(), values.end());
}

float FuncXYEval::zmin(const float values[]) const
{
    return *std::min_element(values, values + size());
}

float FuncXYEval::zmax(const float values[]) const
{
    return *std::max_element(values, values + size());
}


void FuncXYEval::evaluate(std::vector<float> &values,
                          const float zscale) const
{
    values.resize(this->size());
    evaluate(values.data(), zscale);
}

void FuncXYEval::evaluate(float values[],
                          const float zscale) const
{
    for (size_t i = 0; i < m_grid.Nx(); i++) {
        for (size_t j = 0; j < m_grid.Ny(); j++) {
            float x = m_grid.xi(i, j);
            float y = m_grid.yi(i, j);
            float z = m_func(x, y);

            values[m_grid.idx(i, j)] = z*zscale;
        }
    }
}


void FuncXYEval::fill_xy(std::vector<Point2D> &xy) const
{
    xy.resize(size());
    fill_xy(xy.data());
}

void FuncXYEval::fill_xy(std::vector<float> &x,
                     std::vector<float> &y) const
{
    x.resize(size());
    y.resize(size());
    fill_xy(x.data(), y.data());
}

void FuncXYEval::fill_xy(Point2D xy[]) const
{
    for (size_t i = 0; i < m_grid.Nx(); i++) {
        for (size_t j = 0; j < m_grid.Ny(); j++) {
            xy[m_grid.idx(i, j)] = m_grid(i, j);
        }
    }
}

void FuncXYEval::fill_xy(float x[], float y[]) const
{
    for (size_t i = 0; i < m_grid.Nx(); i++) {
        for (size_t j = 0; j < m_grid.Ny(); j++) {
            x[m_grid.idx(i, j)] = m_grid.xi(i, j);
            y[m_grid.idx(i, j)] = m_grid.yi(i, j);
        }
    }
}

// -------------------------------------------------------

FuncXY::FuncXY()
    : FuncXYEval(),
      m_values(m_grid.Nx()*m_grid.Ny(), 0.0f)
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

FuncXY::FuncXY(Point2D origin,
               float scalex, float scaley,
               size_t Nx, size_t Ny,
               func2d_t func)
    : FuncXYEval(origin, scalex, scaley, Nx, Ny, func),
      m_values(Nx*Ny, 0.0f)
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

FuncXY::FuncXY(float xmin, float xmax,
               float ymin, float ymax,
               size_t Nx, size_t Ny,
               func2d_t func)
    : FuncXYEval(xmin, xmax, ymin, ymax, Nx, Ny, func),
      m_values(Nx*Ny, 0.0f)
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    std::cout << "  size: " << Nx << " x " << Ny << " = " << Nx*Ny << std::endl;
    std::cout << "  range: ["
              << xmin << "," << xmax << "] x ["
              << ymin << "," << ymax << "]" << std::endl;
}


float FuncXY::zmin() const
{
    return *std::min_element(m_values.begin(), m_values.end());
}

float FuncXY::zmax() const
{
    return *std::max_element(m_values.begin(), m_values.end());
}

void FuncXY::evaluate(const float zscale)
{
    m_values.reserve(size());

    for (size_t i = 0; i < m_grid.Nx(); i++) {
        for (size_t j = 0; j < m_grid.Ny(); j++) {
            float x = m_grid.xi(i, j);
            float y = m_grid.yi(i, j);
            float z = m_func(x, y);

            m_values[m_grid.idx(i, j)] = z*zscale;
        }
    }
}
