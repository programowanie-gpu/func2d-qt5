#ifndef GRID2D_H
#define GRID2D_H

#include <cstdlib>

struct Point2D
{
    float x;
    float y;
};

class Grid2D
{
public:
    Grid2D(Point2D origin, float scalex, float scaley,
           size_t Nx, size_t Ny);
    Grid2D(float xmin, float xmax,
           float ymin, float ymax,
           size_t Nx, size_t Ny);

    // getters and getter-like methods
    Point2D origin() const { return m_origin; }
    float dx() const { return m_dx; }
    float dy() const { return m_dy; }
    float xmin() const { return m_origin.x; }
    float ymin() const { return m_origin.y; }
    float xmax() const { return m_origin.x + (m_Nx-1)*m_dx; }
    float ymax() const { return m_origin.y + (m_Ny-1)*m_dy; }
    size_t Nx() const { return m_Nx; }
    size_t Ny() const { return m_Ny; }

    // setters and setter-like methods
    // TODO: fluent interface???
    void moveTo(Point2D new_origin) { m_origin = new_origin; }
    void moveRel(Point2D delta) { m_origin.x += delta.x; m_origin.y += delta.y; }
    void setOrigin(Point2D origin) { moveTo(origin); }
    void setDx(float dx) { m_dx = dx; }
    void setDy(float dy) { m_dy = dy; }
    // ... (when needed) ...

    // points on grid
    Point2D operator()(int i, int j) const {
        return Point2D{m_origin.x + i*m_dx,
                       m_origin.y + j*m_dy};
    }
    Point2D operator()(size_t i, size_t j) const {
        return Point2D{m_origin.x + i*m_dx,
                       m_origin.y + j*m_dy};
    }
    float xi(int ix) const { return m_origin.x + ix*m_dx; }
    float yi(int iy) const { return m_origin.y + iy*m_dy; }
    float xi(int ix, int) const { return m_origin.x + ix*m_dx; }
    float yi(int, int iy) const { return m_origin.y + iy*m_dy; }
    float xi(size_t ix, size_t) const { return m_origin.x + ix*m_dx; }
    float yi(size_t, size_t iy) const { return m_origin.y + iy*m_dy; }

    // flattened index - decide whether 'x' are in row or col
    // use C-style row-first matrix layout
    size_t idx(size_t ix, size_t iy) const { return m_Nx*ix + iy; }

    // TODO: iterators
    //
    // https://stackoverflow.com/questions/8054273/how-to-implement-an-stl-style-iterator-and-avoid-common-pitfalls
    // https://www.fluentcpp.com/2018/05/08/std-iterator-deprecated/

private:
    // main data, defining infinite grid of points
    Point2D m_origin{0.0f, 0.0f};
    float m_dx = 1.0f;
    float m_dy = 1.0f;

    // supplementary data, defining finite grid
    size_t m_Nx = 1;
    size_t m_Ny = 1;
};

#endif // GRID2D_H
