#include "openglutils.h"

void _check_gl_errors(const char *file, unsigned int line)
{
    GLenum err;
    while ((err = glGetError()) != GL_NO_ERROR) {
        /*
         * If you try to clear the error stack, make sure not just keep going
         * until GL_NO_ERROR is returned but also break on GL_CONTEXT_LOST
         * as that error value will keep repeating.
         */
        if (err == GL_CONTEXT_LOST) {
            fprintf(stderr, "%s:%d glError (repeating?) - %s\n", file, line, "GL_CONTEXT_LOST");
            break; // out of loop
        }

        switch (err) {
        case GL_INVALID_ENUM:
            fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_INVALID_ENUM");
            break;
        case GL_INVALID_VALUE:
            fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_INVALID_VALUE");
            break;
        case GL_INVALID_OPERATION:
            fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_INVALID_OPERATION");
            break;
        case GL_INVALID_FRAMEBUFFER_OPERATION:
            fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_INVALID_FRAMEBUFFER_OPERATION");
            break;
        case GL_OUT_OF_MEMORY:
            fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_OUT_OF_MEMORY");
            break;
        case GL_STACK_UNDERFLOW:
            fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_STACK_UNDERFLOW");
            break;
        case GL_STACK_OVERFLOW:
            fprintf(stderr, "%s:%d glError - %s\n", file, line, "GL_STACK_OVERFLOW");
            break;
        default:
            fprintf(stderr, "%s:%d glError - [%d]\n", file, line, err);
            break;
        }
    }
}
