#include <QtWidgets>

#include <QTimer>
#include <QMouseEvent>
#include <QWheelEvent>

#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>

#include "eulerrecover.h"
#include "plotglwidget.h"

// debugging
#undef NDEBUG
#include <QtGlobal>
#include "openglutils.h"


PlotGLWidget::PlotGLWidget(QWidget *parent) :
    QOpenGLWidget(parent)
{
    qDebug("[%s] start", __PRETTY_FUNCTION__);

    // model rotation
    xAngle = xRot = 0;
    yAngle = yRot = 0;
    zAngle = zRot = 0;

    // camera rotation
    // TODO: names, keep or leave it?
    alpha =  25;
    beta  = -25;

    mouseRotationStep = 2;

    cameraDistance = 2.5;


    lightAngle = 0;
    animate = true;

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &PlotGLWidget::timeout);
    timer->start(20);

    qDebug("[%s] end", __PRETTY_FUNCTION__);
}

PlotGLWidget::~PlotGLWidget()
{
    qDebug("%s", __PRETTY_FUNCTION__);
    cleanup();
}

QSize PlotGLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize PlotGLWidget::sizeHint() const
{
    return QSize(512, 512);
}

void PlotGLWidget::setXRotation(int angle)
{
    qNormalizeAngle(angle);

    if (angle != xRot) {
        xAngle = xRot = angle;
        emit xRotationChanged(angle);
        update();
    }

    // debug
    QMatrix4x4 mMatrix; // model matrix
    mMatrix.rotate(xAngle, 1, 0, 0);
    mMatrix.rotate(yAngle, 0, 1, 0);
    mMatrix.rotate(zAngle, 0, 0, 1);
    eulerAngles(mMatrix, xAngle, yAngle, zAngle);
}

void PlotGLWidget::setYRotation(int angle)
{
    qNormalizeAngle(angle);

    if (angle != yRot) {
        yAngle = yRot = angle;
        emit yRotationChanged(angle);
        update();
    }
}

void PlotGLWidget::setZRotation(int angle)
{
    qNormalizeAngle(angle);

    if (angle != zRot) {
        zAngle = zRot = angle;
        emit zRotationChanged(angle);
        update();
    }
}

void PlotGLWidget::checkDrawSurface(int status)
{
    plot3d.checkDrawSurface = status;
    update();
}

void PlotGLWidget::checkDrawLines(int status)
{
    plot3d.checkDrawLines = status;
    update();
}

void PlotGLWidget::checkDrawPoints(int status)
{
    plot3d.checkDrawPoints = status;
    update();
}

void PlotGLWidget::checkDrawCube(int status)
{
    plot3d.checkDrawCube = status;
    update();
}

void PlotGLWidget::initializeGL()
{
    qDebug("[%s] start", __PRETTY_FUNCTION__);
    // In many cases the widget's corresponding top-level window can change
    // several times during the widget's lifetime. Whenever this happens, the
    // QOpenGLWidget's associated context is destroyed and a new one is created.
    // Therefore we have to be prepared to clean up the resources on the
    // aboutToBeDestroyed() signal, instead of the destructor. The emission of
    // the signal will be followed by an invocation of initializeGL() where we
    // can recreate all resources.
    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &PlotGLWidget::cleanup);

    // Initialize OpenGL function resolution for current context.
    // It makes it possible to simplu use functions from OpenGL.
    // Alternative solution would be to use QOpenGLFunctions object:
    //   QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    //   f->glClear(GL_COLOR_BUFFER_BIT);
    initializeOpenGLFunctions();

#ifndef NDEBUG
    // initialize logger
    QOpenGLContext *ctx = QOpenGLContext::currentContext();
    qDebug("..OpenGL %d.%d", ctx->format().majorVersion(), ctx->format().minorVersion());
    QOpenGLDebugLogger *logger = nullptr;
    if (ctx->hasExtension(QByteArrayLiteral("GL_KHR_debug"))) {
        logger = new QOpenGLDebugLogger(this);
        logger->initialize(); // initializes in the current context, i.e. ctx
        qDebug("..initialized QOpenGLDebugLogger");
    } else {
        qDebug("..could not initialize OpenGL logger: no GL_KHR_debug extension");
    }

    if (logger) {
        // read logged messages, if any
        const QList<QOpenGLDebugMessage> messages = logger->loggedMessages();
        qDebug("..reading OpenGL message log: %d elements", messages.size());
        for (const QOpenGLDebugMessage &message : messages)
            qDebug() << message;

        // turn on real-time logging
        connect(logger, &QOpenGLDebugLogger::messageLogged,
                [](const QOpenGLDebugMessage &debugMessage) {
            qDebug() << debugMessage;
        });
        // glLineWidth with width greater than 1.0 is deprecated (?), ignore it
        logger->disableMessages(QOpenGLDebugMessage::AnySource,
                                QOpenGLDebugMessage::DeprecatedBehaviorType);
        logger->startLogging();
        qDebug("..started real-time OpenGL logging");
    }
#endif /* !defined(NDEBUG) */

    // replaces qglClearColor(QColor(Qt::black)); from QtOpenGL
    glClearColor(0, 0, 0, 1); // black, opaque

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    plot3d.initializeGL();


    coloringShaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex,   ":/coloringVertexShader.vsh");
    coloringShaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment, ":/coloringFragmentShader.fsh");
    coloringShaderProgram.link();
    qDebug("..created and linked coloringShaderProgram [%d]", coloringShaderProgram.programId());

    QVector<QVector3D> spotlightVertices;
    QVector<QVector3D> spotlightColors;

    spotlightVertices
                       << QVector3D(   0,    1,    0) << QVector3D(-0.5,    0,  0.5) << QVector3D( 0.5,    0,  0.5) // Front
                       << QVector3D(   0,    1,    0) << QVector3D( 0.5,    0, -0.5) << QVector3D(-0.5,    0, -0.5) // Back
                       << QVector3D(   0,    1,    0) << QVector3D(-0.5,    0, -0.5) << QVector3D(-0.5,    0,  0.5) // Left
                       << QVector3D(   0,    1,    0) << QVector3D( 0.5,    0,  0.5) << QVector3D( 0.5,    0, -0.5) // Right
                       << QVector3D(-0.5,    0, -0.5) << QVector3D( 0.5,    0, -0.5) << QVector3D( 0.5,    0,  0.5) // Bottom
                       << QVector3D( 0.5,    0,  0.5) << QVector3D(-0.5,    0,  0.5) << QVector3D(-0.5,    0, -0.5);
     spotlightColors
                     << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) // Front
                     << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) // Back
                     << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) // Left
                     << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) << QVector3D(0.2, 0.2, 0.2) // Right
                     << QVector3D(  1,   1,   1) << QVector3D(  1,   1,   1) << QVector3D(  1,   1,   1) // Bottom
                     << QVector3D(  1,   1,   1) << QVector3D(  1,   1,   1) << QVector3D(  1,   1,   1);

     numSpotlightVertices = spotlightVertices.size();

     spotlightBuffer.create();
     spotlightBuffer.bind();
     // 3d vector for vertices + RGB for colors
     spotlightBuffer.allocate(numSpotlightVertices * (3 + 3) * sizeof(GLfloat));

     int offset = 0;
     spotlightBuffer.write(offset, spotlightVertices.constData(), numSpotlightVertices * 3 * sizeof(GLfloat));
     offset += numSpotlightVertices * 3 * sizeof(GLfloat);
     spotlightBuffer.write(offset, spotlightColors.constData(),   numSpotlightVertices * 3 * sizeof(GLfloat));

     spotlightBuffer.release();
     qDebug("..created and filled spotlightBuffer VBO [%d]", spotlightBuffer.bufferId());

     qDebug("[%s] end", __PRETTY_FUNCTION__);
}

void PlotGLWidget::cleanup()
{
    qDebug("[%s] start", __PRETTY_FUNCTION__);

    makeCurrent();
    spotlightBuffer.destroy();
    plot3d.cleanup();
    doneCurrent();

    qDebug("[%s] end", __PRETTY_FUNCTION__);
}

void PlotGLWidget::resizeGL(int width, int height)
{
    if (height == 0) {
        height = 1; // to avoid division by zero
    }

    pMatrix.setToIdentity();
    pMatrix.perspective(60.0f, static_cast<float>(width) / height, 0.001f, 1000);

    glViewport(0, 0, width, height);
}

void PlotGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    QMatrix4x4 mMatrix; // model matrix
    QMatrix4x4 vMatrix; // view matrix

    // model transformation (objects)

    // Tait-Bryan angles (nautical angles, or Cardan angles)
    // z-y'-x'' - yaw, pitch, roll  or  heading, elevation, bank
    // rotation angle in degrees (0..360 or -180..180)
    mMatrix.rotate(xAngle, 1, 0, 0);
    mMatrix.rotate(yAngle, 0, 1, 0);
    mMatrix.rotate(zAngle, 0, 0, 1);

    // view transformation (camera)

    QMatrix4x4 cameraTransformation;
    cameraTransformation.rotate(alpha, 0, 1, 0);
    cameraTransformation.rotate(beta,  1, 0, 0);

    QVector3D cameraPosition    = cameraTransformation * QVector3D(0, 0, cameraDistance);
    QVector3D cameraUpDirection = cameraTransformation * QVector3D(0, 1, 0);

    vMatrix.lookAt(cameraPosition, QVector3D(0, 0, 0), cameraUpDirection);

    // lighting: model-view transformation, normals,...

    QMatrix4x4 mvMatrix;
    mvMatrix = vMatrix * mMatrix;

    QMatrix3x3 normalMatrix;
    normalMatrix = mvMatrix.normalMatrix();

    QMatrix4x4 lightTransformation;
    lightTransformation.rotate(lightAngle, 0, 1, 0);

    QVector3D lightPosition = lightTransformation * QVector3D(0, 1, 1);

    // we want to see both sides of surface
    glDisable(GL_CULL_FACE);
    glFrontFace(GL_CW);  // because of how mesh is constructed, front side has clock-wise winding
    plot3d.draw(mMatrix, vMatrix, pMatrix, lightPosition);
    glFrontFace(GL_CCW); // restore the default
    glEnable(GL_CULL_FACE);


    // drawing camera "object" with flat color

    mMatrix.setToIdentity();
    mMatrix.translate(lightPosition);
    mMatrix.rotate(lightAngle, 0, 1, 0);
    mMatrix.rotate(45, 1, 0, 0);
    mMatrix.scale(0.1f);

    coloringShaderProgram.bind();

    coloringShaderProgram.setUniformValue("mvpMatrix", pMatrix * vMatrix * mMatrix);

    spotlightBuffer.bind();
    int offset = 0;
    coloringShaderProgram.setAttributeBuffer("vertex", GL_FLOAT, offset, 3, 0);
    coloringShaderProgram.enableAttributeArray("vertex");
    offset += numSpotlightVertices * 3 * sizeof(GLfloat);
    coloringShaderProgram.setAttributeBuffer("color", GL_FLOAT, offset, 3, 0);
    coloringShaderProgram.enableAttributeArray("color");
    spotlightBuffer.release();

    glDrawArrays(GL_TRIANGLES, 0, numSpotlightVertices);

    coloringShaderProgram.disableAttributeArray("color");
    coloringShaderProgram.disableAttributeArray("vertex");

    coloringShaderProgram.release();
}


void PlotGLWidget::mousePressEvent(QMouseEvent *event)
{
    lastMousePosition = event->pos();

    event->accept();
}

void PlotGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int deltaX = event->x() - lastMousePosition.x();
    int deltaY = event->y() - lastMousePosition.y();

    if (event->buttons() & Qt::LeftButton) {
        setXRotation(xRot + mouseRotationStep * deltaY);
        setYRotation(yRot + mouseRotationStep * deltaX);

        update();

    } else if (event->buttons() & Qt::RightButton) {
        setXRotation(xRot + mouseRotationStep * deltaY);
        setZRotation(zRot + mouseRotationStep * deltaX);

        update();
    }

    lastMousePosition = event->pos();

    event->accept();
}

void PlotGLWidget::wheelEvent(QWheelEvent *event)
{
    int delta = event->delta();

    if (event->orientation() == Qt::Vertical) {
        if (delta < 0) {
            cameraDistance *= 1.1f;
        } else if (delta > 0) {
            cameraDistance *= 0.9f;
        }

        emit cameraDistanceChanged(cameraDistance);
        update();
    }

    event->accept();
}

void PlotGLWidget::timeout()
{
    if (!animate)
        return;

    lightAngle += 1;
    while (lightAngle >= 360) {
        lightAngle -= 360;
    }

    update();
}
