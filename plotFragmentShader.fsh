#version 130

uniform vec4 ambientColor;
uniform vec4 diffuseColor;
uniform vec4 specularColor;
uniform float ambientReflection;
uniform float diffuseReflection;
uniform float specularReflection;
uniform float shininess;

in vec4 varyingColor;
in vec3 varyingLightDirection;
in vec3 varyingViewerDirection;

// output color as RGBA of a fragment's pixel,
// to be used in place of gl_FragColor built-in variable
out vec4 fragColor;

void main(void)
{
    /*
    vec3 xTangent = dFdx(varyingViewerDirection);
    vec3 yTangent = dFdy(varyingViewerDirection);
    vec3 varyingNormal = cross(xTangent, yTangent);

    vec3 normal          = normalize(varyingNormal);
    vec3 lightDirection  = normalize(varyingLightDirection);
    vec3 viewerDirection = normalize(varyingViewerDirection);

    vec4 ambientIllumination = ambientReflection*ambientColor;
    vec4 diffuseIllumination = diffuseReflection*max(0.0, dot(lightDirection, normal))*diffuseColor;
    vec4 specularIllumination =
        specularReflection*pow(max(0.0, dot(-reflect(lightDirection, normal), viewerDirection)), shininess)*
        specularColor;

    fragColor = varyingColor*(ambientIllumination + diffuseIllumination)
              + specularIllumination;
    */

    // make back-side darker
    float factor;

    if (gl_FrontFacing)
            factor = 1.0;
    else
            factor = 0.5;


    fragColor = factor * ambientColor * varyingColor;
}


