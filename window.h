#ifndef WINDOW_H
#define WINDOW_H

#include "plotglwidget.h"

#include <QCheckBox>
#include <QDial>
#include <QSlider>
#include <QWidget>


class Window : public QWidget
{
    Q_OBJECT
    
public:
    explicit Window(QWidget *parent = nullptr);
    ~Window();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    // control widgets slots
    void checkAnimateLight(int checkedStatus);
    // other slots are on PlotGLWidget class

private:
    // control widgets
    QDial *xRotationDial;
    QDial *yRotationDial;
    QDial *zRotationDial;
    QSlider *xRotationSlider;
    QSlider *yRotationSlider;
    QSlider *zRotationSlider;
    QCheckBox *animateLightCheckBox;
    QCheckBox *drawSurfaceCheckBox;
    QCheckBox *drawLinesCheckBox;
    QCheckBox *drawPointsCheckBox;
    QCheckBox *drawCubeCheckBox;
    PlotGLWidget *plotGLWidget;
};

#endif // WINDOW_H
