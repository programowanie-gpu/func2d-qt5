#ifndef FUNCXY_H
#define FUNCXY_H

#include <vector>
#include <functional>

#include "grid2d.h"

using func2d_t = std::function<float(float,float)>;

/**
 * @brief The FuncXYEval class represents f(x,y) function on regular grid
 *
 * This class does not include storage; all evaluation results are put
 * into data storage provided as parameter to the evaluate() method.
 */
class FuncXYEval
{
public:
    FuncXYEval(); // default function, default range
    FuncXYEval(Point2D origin,
               float scalex, float scaley,
               size_t Nx, size_t Ny,
               func2d_t func);
    FuncXYEval(float xmin, float xmax,
               float ymin, float ymax,
               size_t Nx, size_t Ny,
               func2d_t func);

    // getters and getter-like methods
    const Grid2D &grid() const { return m_grid; }
    size_t size() const { return m_grid.Nx()*m_grid.Ny(); }

    // return grid range
    float xmin() const { return m_grid.xmin(); }
    float xmax() const { return m_grid.xmax(); }
    float ymin() const { return m_grid.ymin(); }
    float ymax() const { return m_grid.ymax(); }

    // return range of evaluated values
    float zmin(const std::vector<float> &values) const;
    float zmax(const std::vector<float> &values) const;
    // unsafe version (no bounds checking)
    float zmin(const float values[]) const;
    float zmax(const float values[]) const;

    // fill z=f(x,y) values
    void evaluate(std::vector<float> &z, const float zscale = 1.0f) const;
    // unsafe version (no bounds checking)
    void evaluate(float z[], const float zscale = 1.0f) const;

    // fill (x, y) values, constant for grid
    void fill_xy(std::vector<Point2D> &xy) const;
    void fill_xy(std::vector<float> &x,
                 std::vector<float> &y) const;
    // unsafe version of filling (x, y) values of grid
    void fill_xy(Point2D xy[]) const;
    void fill_xy(float x[], float y[]) const;

protected:
    Grid2D m_grid;  
    func2d_t m_func;
};


/**
 * @brief The FuncXY class represents f(x,y) function evaluated on grid
 *
 * This class allocates data storage for function values, and stores them,
 * as opposed to the FuncXYEval base class.
 */
class FuncXY : public FuncXYEval
{
public:
    FuncXY(); // default function, default range
    FuncXY(Point2D origin,
           float scalex, float scaley,
           size_t Nx, size_t Ny,
           func2d_t func);
    FuncXY(float xmin, float xmax,
           float ymin, float ymax,
           size_t Nx, size_t Ny,
           func2d_t func);

    // getters and getter-like methods
    const float *data() const { return m_values.data(); }
    const std::vector<float> &values() const { return m_values; }

    // return range of evaluated and stored values
    float zmin() const;
    float zmax() const;

    // fill values with f(x,y) at grid points
    void evaluate(const float zscale = 1.0f);

protected:
    std::vector<float> m_values;
};

#endif // FUNCXY_H
