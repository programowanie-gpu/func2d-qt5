#ifndef MESH2DGL_H
#define MESH2DGL_H

#include <vector>

#include <QOpenGLBuffer>
#include <QOpenGLFunctions>

#include "grid2d.h"


class Mesh2DGL : protected QOpenGLFunctions
{
public:
    Mesh2DGL() { m_indexBuffer.create(); m_indexBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw); }
    virtual ~Mesh2DGL() { qDebug("%s", __PRETTY_FUNCTION__); destroy(); }

    // void glDrawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid *indices);
    // - mode: Specifies what kind of primitives to render.
    // - count: Specifies the number of elements to be rendered.
    // - type: Specifies the type of the values in `indices`.
    // - indices: Specifies an offset of the first index in the array
    //   in the data store of the buffer currently bound to
    //   the GL_ELEMENT_ARRAY_BUFFER target.
    virtual GLenum draw_mode() const = 0;
    virtual GLsizei count() const { return static_cast<GLsizei>(m_indexData.size()); }
    virtual GLenum index_type() const { return GL_UNSIGNED_INT; }

    virtual void initializeGL();
    virtual void drawElements();
    virtual void destroy() { m_indexBuffer.destroy(); }

protected:
    QOpenGLBuffer m_indexBuffer{QOpenGLBuffer::IndexBuffer};
    std::vector<GLuint> m_indexData;
};


namespace Surface {
    // this class is here only for inheritance chain, to distinguist types of meshes
    class Mesh : public Mesh2DGL {
    public:
        Mesh() : Mesh2DGL() {}
        virtual ~Mesh() { qDebug("%s", __PRETTY_FUNCTION__); }

        virtual GLenum index_type() const;
    };

    class MeshTriangles : public Mesh
    {
    public:
        MeshTriangles(GLuint Nx, GLuint Ny);
        explicit MeshTriangles(const Grid2D &grid)
            : MeshTriangles(static_cast<GLuint>(grid.Nx()), static_cast<GLuint>(grid.Ny())) {}

        virtual GLenum draw_mode() const override;
    };

    // this class is here only for inheritance chain, to distinguist types of grids
    class Grid : public Mesh2DGL {
    public:
        Grid() : Mesh2DGL() {}
        virtual ~Grid() { qDebug("%s", __PRETTY_FUNCTION__); }

        virtual GLenum index_type() const;
    };

    class GridLines : public Grid
    {
    public:
        GridLines(GLuint Nx, GLuint Ny);
        explicit GridLines(const Grid2D &grid)
            : GridLines(static_cast<GLuint>(grid.Nx()), static_cast<GLuint>(grid.Ny())) {}

        virtual GLenum draw_mode() const override;
    };
} // namespace Surface

#endif // MESH2DGL_H
