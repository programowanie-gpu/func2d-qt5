#include <QtWidgets>

#include "window.h"

#include "plotglwidget.h"

Window::Window(QWidget *parent) :
    QWidget(parent)
{
    qDebug("[%s] start", __PRETTY_FUNCTION__);

    // create widgets, labels and layouts
    // - create rotation dials and their labels
    xRotationDial = new QDial;
    yRotationDial = new QDial;
    zRotationDial = new QDial;
    QLabel *xRotationLabel = new QLabel("xrot");
    QLabel *yRotationLabel = new QLabel("yrot");
    QLabel *zRotationLabel = new QLabel("zrot");
    for (auto dial : { xRotationDial, yRotationDial, zRotationDial }) {
        dial->setWrapping(true);

        dial->setNotchTarget(3.6);
        dial->setNotchesVisible(true);

        dial->setRange(0, 360);
        dial->setSingleStep(5);
        dial->setPageStep(15);
    }

    QGridLayout *dialsLayout = new QGridLayout;
    dialsLayout->setRowStretch(0, 1);
    dialsLayout->addWidget(xRotationDial, 0, 0);
    dialsLayout->addWidget(yRotationDial, 0, 1);
    dialsLayout->addWidget(zRotationDial, 0, 2);
    dialsLayout->addWidget(xRotationLabel, 1, 0, Qt::AlignHCenter);
    dialsLayout->addWidget(yRotationLabel, 1, 1, Qt::AlignHCenter);
    dialsLayout->addWidget(zRotationLabel, 1, 2, Qt::AlignHCenter);

    // - create checkboxes, and their layout
    animateLightCheckBox = new QCheckBox("animate light");
    animateLightCheckBox->setChecked(true);
    drawSurfaceCheckBox = new QCheckBox("draw surface");
    drawLinesCheckBox = new QCheckBox("draw lines (grid)");
    drawPointsCheckBox = new QCheckBox("draw point cloud");
    drawCubeCheckBox = new QCheckBox("show bounding cube");
    QVBoxLayout *sideLayout = new QVBoxLayout;
    // tight spacing
    sideLayout->setSpacing(0);
    sideLayout->setMargin(0);
    // add widgets
    sideLayout->addWidget(animateLightCheckBox);
    sideLayout->addWidget(drawSurfaceCheckBox);
    sideLayout->addWidget(drawLinesCheckBox);
    sideLayout->addWidget(drawPointsCheckBox);
    sideLayout->addWidget(drawCubeCheckBox);
    // widgets aligned to top
    sideLayout->addStretch(1.0);

    // - create dials+checkbox layout
    QHBoxLayout *horizontalLayout = new QHBoxLayout;
    horizontalLayout->addLayout(dialsLayout);
    //horizontalLayout->addWidget(animateLightCheckBox);
    horizontalLayout->addLayout(sideLayout);
    horizontalLayout->setSpacing(15);

    // - create rotations sliders and their layout
    xRotationSlider = new QSlider(Qt::Horizontal);
    yRotationSlider = new QSlider(Qt::Horizontal);
    zRotationSlider = new QSlider(Qt::Horizontal);
    for (auto slider : { xRotationSlider, yRotationSlider, zRotationSlider }) {
        slider->setRange(0, 360);
        slider->setSingleStep(5);
        slider->setPageStep(15);

        slider->setTickInterval(15);
        slider->setTickPosition(QSlider::TicksAbove);
    }
    QFormLayout *formLayout = new QFormLayout;
    formLayout->addRow("&xrot", xRotationSlider);
    formLayout->addRow("&yrot", yRotationSlider);
    formLayout->addRow("&zrot", zRotationSlider);

    // - put rotation sliders below rotation dials
    QVBoxLayout *controlsLayout = new QVBoxLayout;
    controlsLayout->addLayout(horizontalLayout);
    controlsLayout->addLayout(formLayout);

    // - add description how to change camera distance
    QLabel *cameraDistanceHelp =
        new QLabel("use scroll wheel to change camera distance (zoom)");
    controlsLayout->addWidget(cameraDistanceHelp);

    // create  group box with all the control widgets
    QGroupBox *controlsGroupBox = new QGroupBox("Controls", this);
    controlsGroupBox->setLayout(controlsLayout);

    // create OpenGL widget
    plotGLWidget = new PlotGLWidget(this);
    plotGLWidget->resize(512, 512);
    // use it to set some defaults
    drawSurfaceCheckBox->setChecked(plotGLWidget->plot().checkDrawSurface);
    drawLinesCheckBox->setChecked(plotGLWidget->plot().checkDrawLines);
    drawPointsCheckBox->setChecked(plotGLWidget->plot().checkDrawPoints);
    drawCubeCheckBox->setChecked(plotGLWidget->plot().checkDrawCube);

    // create window layout, set window size
    QVBoxLayout *windowLayout = new QVBoxLayout;
    windowLayout->addWidget(plotGLWidget);
    windowLayout->addWidget(controlsGroupBox);
    setLayout(windowLayout);
    resize(530, 735+95);

    // connect control widgets
    connect(animateLightCheckBox, &QCheckBox::stateChanged, this, &Window::checkAnimateLight);
    connect(drawSurfaceCheckBox, &QCheckBox::stateChanged, plotGLWidget, &PlotGLWidget::checkDrawSurface);
    connect(drawLinesCheckBox, &QCheckBox::stateChanged, plotGLWidget, &PlotGLWidget::checkDrawLines);
    connect(drawPointsCheckBox, &QCheckBox::stateChanged, plotGLWidget, &PlotGLWidget::checkDrawPoints);
    connect(drawCubeCheckBox, &QCheckBox::stateChanged, plotGLWidget, &PlotGLWidget::checkDrawCube);

    connect(xRotationDial, &QAbstractSlider::valueChanged, plotGLWidget, &PlotGLWidget::setXRotation);
    connect(yRotationDial, &QAbstractSlider::valueChanged, plotGLWidget, &PlotGLWidget::setYRotation);
    connect(zRotationDial, &QAbstractSlider::valueChanged, plotGLWidget, &PlotGLWidget::setZRotation);

    connect(xRotationSlider, &QAbstractSlider::valueChanged, plotGLWidget, &PlotGLWidget::setXRotation);
    connect(yRotationSlider, &QAbstractSlider::valueChanged, plotGLWidget, &PlotGLWidget::setYRotation);
    connect(zRotationSlider, &QAbstractSlider::valueChanged, plotGLWidget, &PlotGLWidget::setZRotation);

    // connect rotation change with all respective rotation control widgets
    connect(plotGLWidget, &PlotGLWidget::xRotationChanged, xRotationSlider, &QAbstractSlider::setValue);
    connect(plotGLWidget, &PlotGLWidget::yRotationChanged, yRotationSlider, &QAbstractSlider::setValue);
    connect(plotGLWidget, &PlotGLWidget::zRotationChanged, zRotationSlider, &QAbstractSlider::setValue);

    connect(plotGLWidget, &PlotGLWidget::xRotationChanged, xRotationDial, &QAbstractSlider::setValue);
    connect(plotGLWidget, &PlotGLWidget::yRotationChanged, yRotationDial, &QAbstractSlider::setValue);
    connect(plotGLWidget, &PlotGLWidget::zRotationChanged, zRotationDial, &QAbstractSlider::setValue);
    qDebug("[%s] end", __PRETTY_FUNCTION__);
}

Window::~Window()
{
    qDebug("%s", __PRETTY_FUNCTION__);
    // ...
}

void Window::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        close();
    else
        QWidget::keyPressEvent(e);
}

void Window::checkAnimateLight(int checkedStatus)
{
    plotGLWidget->animate = !(checkedStatus == 0);
}
