#version 130

uniform mat4 mvpMatrix;
uniform mat4 mvMatrix;
uniform vec3 lightPosition;

in vec2 vertexXY;
in float vertexZ;
uniform vec4 color;

out vec4 varyingColor;

out vec3 varyingLightDirection;
out vec3 varyingViewerDirection;

// A collection of GLSL fragment shaders to draw color maps.
//   https://github.com/kbinani/glsl-colormap/
// The MIT License.
vec4 colormap(float x);

void main(void)
{
    /*
    vec4 vertex;
    vertex.xy = vertexXY;
    vertex.z  = vertexZ;
    vertex.w  = 1.0;

    vec4 eyeVertex = mvMatrix * vertex;
    eyeVertex /= eyeVertex.w;

    varyingColor           = color;
    varyingLightDirection  =  lightPosition - eyeVertex.xyz;
    varyingViewerDirection = -eyeVertex.xyz;

    gl_Position = mvpMatrix * vertex;
    */

    gl_Position = mvpMatrix * vec4(vertexXY, vertexZ, 1);
    varyingColor = colormap(0.5*vertexZ + 0.5); // -1..1 -> 0..1
}


// https://github.com/kbinani/glsl-colormap/blob/master/shaders/MATLAB_jet.frag
float colormap_red(float x) {
    if (x < 0.7) {
        return  4.0 * x - 1.5;
    } else {
        return -4.0 * x + 4.5;
    }
}

float colormap_green(float x) {
    if (x < 0.5) {
        return  4.0 * x - 0.5;
    } else {
        return -4.0 * x + 3.5;
    }
}

float colormap_blue(float x) {
    if (x < 0.3) {
        return  4.0 * x + 0.5;
    } else {
        return -4.0 * x + 2.5;
    }
}

vec4 colormap(float x) {
    float r = clamp(colormap_red(x),   0.0, 1.0);
    float g = clamp(colormap_green(x), 0.0, 1.0);
    float b = clamp(colormap_blue(x),  0.0, 1.0);

    return vec4(r, g, b, 1.0);
}
