#include "grid2d.h"

// debug
#include <iostream>
#include <iomanip>


Grid2D::Grid2D(Point2D origin, float scalex, float scaley,
               size_t Nx, size_t Ny)
    : m_origin{origin},
      m_dx{scalex},
      m_dy{scaley},
      m_Nx{Nx},
      m_Ny{Ny}
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

Grid2D::Grid2D(float xmin, float xmax,
               float ymin, float ymax,
               size_t Nx, size_t Ny)
    : m_origin{xmin, ymin},
      m_Nx{Nx},
      m_Ny{Ny}
{
    // we want xmax == xmin + (Nx-1)*dx, etc.
    // if Nx <= 1, then dx doesn't matter, can be 1.0f
    if (Nx > 1)
        m_dx = (xmax - xmin)/(Nx-1);
    if (Ny > 1)
        m_dy = (ymax - ymin)/(Ny-1);

    std::cout << __PRETTY_FUNCTION__ << std::endl;
    std::cout << "  size: " << Nx << " x " << Ny << std::endl;
    std::cout << "  range: ["
              << xmin << "," << xmax << "] x ["
              << ymin << "," << ymax << "]" << std::endl;

    std::cout << "  origin= (" << m_origin.x << "," << m_origin.y << ")" << std::endl;
    std::cout << "  delta= " << m_dx << " x " << m_dy << std::endl;
}


