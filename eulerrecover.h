#ifndef EULERRECOVER_H
#define EULERRECOVER_H

#include <QMatrix4x4>


void qNormalizeAngle(int &angleDeg);
float qNormalizedAngle(float angleDeg);

void eulerAngles(const QMatrix4x4 &mMatrix, float &xAngle, float &yAngle, float &zAngle);

#endif // EULERRECOVER_H
