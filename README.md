func2d-qt5 -- Plot f(x,y) using OpenGL i Qt
===========================================

Draws surface plot of a function of two parameters
$z = f(x,y)$.

**NOTE:** Currently does not use CUDA

![screenshot](screenshots/screenshot-plot3d-v2.png)
