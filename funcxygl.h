#ifndef FUNCXYGL_H
#define FUNCXYGL_H

#include <QOpenGLBuffer>

#include "funcxy.h"

void evaluate(const FuncXYEval &func, QOpenGLBuffer &vbo);
void evaluate(FuncXY &func, QOpenGLBuffer &vbo);

namespace BufferWrapper {
    template<class FuncXYType>
    class FuncXY {
    public:
        FuncXY(FuncXYType &func) : m_func_ref{func} {}

        bool create() { return m_buffer.create(); }
        void initializeGL();
        void evaluate();
        bool bind() { return m_buffer.bind(); }
        void release() { m_buffer.release(); }
        void destroy();

        GLuint bufferId() const { return m_buffer.bufferId(); }
        bool isCreated() const { return m_buffer.isCreated(); }
        int size() const { return m_buffer.size(); }

    private:
        QOpenGLBuffer m_buffer{QOpenGLBuffer::VertexBuffer};
        FuncXYType &m_func_ref;
    };


    template<class FuncXYType>
    void FuncXY<FuncXYType>::initializeGL()
    {
        qDebug("[%s] start", __PRETTY_FUNCTION__);

        m_buffer.create();
        m_buffer.bind();
        size_t sizeZ = m_func_ref.size() * 1 * sizeof(float);
        m_buffer.allocate(static_cast<int>(sizeZ));
        m_buffer.release();
        qDebug("  created [%d] VBO for %s, and allocated %zd bytes",
               m_buffer.bufferId(), typeid(m_func_ref).name(), sizeZ);

        qDebug("[%s] end", __PRETTY_FUNCTION__);
    }

    template<class FuncXYType>
    void FuncXY<FuncXYType>::evaluate()
    {
        ::evaluate(m_func_ref, m_buffer);
    }

    template<class FuncXYType>
    void FuncXY<FuncXYType>::destroy()
    {
        qDebug("[%s] start", __PRETTY_FUNCTION__);

        if (!m_buffer.isCreated() || m_buffer.bufferId() == 0) {
            qDebug("[%s] early end", __PRETTY_FUNCTION__);
        }
        qDebug("  about to destroy VBO [%d]", m_buffer.bufferId());
        m_buffer.destroy();

        qDebug("[%s] end", __PRETTY_FUNCTION__);
    }
}

template class BufferWrapper::FuncXY<FuncXYEval>;
template class BufferWrapper::FuncXY<FuncXY>;

#endif // FUNCXYGL_H
