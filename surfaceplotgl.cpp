#include "surfaceplotgl.h"

#include <QColor>
#include "funcxygl.h"

// debugging
#undef NDEBUG
#include <QtDebug>
#include "openglutils.h"


SurfacePlotGL::SurfacePlotGL(float xmin, float xmax,
                             float ymin, float ymax,
                             GLuint Nx, GLuint Ny,
                             func2d_t func)
    : m_plottedFunction(xmin, xmax,
                        ymin, ymax,
                        Nx, Ny, func),
      m_wrappedFunction{m_plottedFunction},
      m_surfaceMesh{new Surface::MeshTriangles(Nx, Ny)},
      m_surfaceGrid{new Surface::GridLines(Nx, Ny)}
{
    qDebug("%s", __PRETTY_FUNCTION__);
}

void SurfacePlotGL::initializeGL()
{
    qDebug("[%s] start", __PRETTY_FUNCTION__);

    initializeOpenGLFunctions();

    // create and initialize buffers for mesh and grid
    m_surfaceMesh->initializeGL();
    m_surfaceGrid->initializeGL();
    // create and initialize buffers for function values
    m_wrappedFunction.initializeGL();

    // create shader program for $z = f(x,y)$ plot
    m_plotShaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                            ":/plotVertexShader.vsh");
    m_plotShaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                            ":/plotFragmentShader.fsh");
    if (!m_plotShaderProgram.link()) {
        qDebug() << "SurfacePlotGL: could not compile plot shader program" << endl
                 << m_plotShaderProgram.log();
    }
    qDebug("  built plot shader program for SurfacePlotGL [%d]", m_plotShaderProgram.programId());

    // create shader program for ancillary elements, like bounding box
    m_coloringShaderProgram.addShaderFromSourceFile(QOpenGLShader::Vertex,
                                                    ":/coloringVertexShader.vsh");
    m_coloringShaderProgram.addShaderFromSourceFile(QOpenGLShader::Fragment,
                                                    ":/coloringFragmentShader.fsh");
    if (!m_coloringShaderProgram.link()) {
        qDebug() << "SurfacePlotGL: could not compile cube shader program" << endl
                 << m_coloringShaderProgram.log();
    }
    qDebug("  built cube shader program for SurfacePlotGL [%d]", m_coloringShaderProgram.programId());

    // create data for per-vertex parameters
    // - (x,y) do not depend on function, do not change
    std::vector<Point2D> verticesXY;
    m_plottedFunction.fill_xy(verticesXY);
    int sizeXY = m_plottedFunction.size()*2*sizeof(GLfloat);
    qDebug("  filled verticesXY, size = %d bytes (%zd x %zd = %zd elements)",
           sizeXY, m_plottedFunction.grid().Nx(), m_plottedFunction.grid().Ny(), m_plottedFunction.size());

    m_vertexXY.create();
    m_vertexXY.bind();
    m_vertexXY.allocate(verticesXY.data(), sizeXY);
    //m_vertexXY.write(offset, verticesXY.data(), sizeXY);
    m_vertexXY.release();
    qDebug("  created and filled vertexXY VBO [%d] (defined range: [%g, %g] x [%g, %g])",
           m_vertexXY.bufferId(),
           m_plottedFunction.grid().xmin(), m_plottedFunction.grid().xmax(),
           m_plottedFunction.grid().ymin(), m_plottedFunction.grid().ymax());
    qDebug("  - 1st  point at (%g, %g)", verticesXY[0].x, verticesXY[0].y);
    qDebug("  - 2nd  point at (%g, %g)", verticesXY[1].x, verticesXY[1].y);
    qDebug("  - last point at (%g, %g) index: %zd",
           verticesXY[verticesXY.size()-1].x, verticesXY[verticesXY.size()-1].y,
           verticesXY.size());

    // - (z) depend on function, will change for f(x,y;t)
    int sizeZ = m_plottedFunction.size()*1*sizeof(GLfloat);
    qDebug("  function, size = %d bytes", sizeZ);

    m_wrappedFunction.evaluate();
    qDebug("  filled m_wrappedFunction VBO [%d], range of values: [%g, %g]",
           m_wrappedFunction.bufferId(),
           m_plottedFunction.zmin(), m_plottedFunction.zmax());

    qDebug("[%s] end", __PRETTY_FUNCTION__);
}

void SurfacePlotGL::draw(QMatrix4x4 mMatrix,
                         QMatrix4x4 vMatrix,
                         QMatrix4x4 pMatrix,
                         QVector3D lightPosition)
{
    // calculate transformation matrices
    QMatrix4x4 mvMatrix{ vMatrix * mMatrix};
    QMatrix4x4 mvpMatrix{pMatrix * mvMatrix};

    // select shader program
    m_plotShaderProgram.bind();

    // set values of uniform parameters
    // - transformation, and extra transformation
    m_plotShaderProgram.setUniformValue("mvpMatrix", mvpMatrix);
    m_plotShaderProgram.setUniformValue("mvMatrix",  mvMatrix);

    m_plotShaderProgram.setUniformValue("lightPosition", vMatrix * lightPosition);
    m_plotShaderProgram.setUniformValue("color", QColor(0, 255, 0));

    // - lightning and shading parameters
    m_plotShaderProgram.setUniformValue("ambientColor",  QColor( 32,  32,  32));
    m_plotShaderProgram.setUniformValue("diffuseColor",  QColor(128, 128, 128));
    m_plotShaderProgram.setUniformValue("specularColor", QColor(255, 255, 255));
    m_plotShaderProgram.setUniformValue("ambientReflection",  static_cast<GLfloat>(1.0f));
    m_plotShaderProgram.setUniformValue("diffuseReflection",  static_cast<GLfloat>(1.0f));
    m_plotShaderProgram.setUniformValue("specularReflection", static_cast<GLfloat>(1.0f));
    m_plotShaderProgram.setUniformValue("shininess", static_cast<GLfloat>(100.0f));

    // set values of per-vertex parameters
    // - positions[, normals, colors, texturing]
    m_vertexXY.bind();
    m_plotShaderProgram.setAttributeBuffer("vertexXY", GL_FLOAT, 0, 2);
    m_plotShaderProgram.enableAttributeArray("vertexXY");
    m_vertexXY.release();

    m_wrappedFunction.bind();
    m_plotShaderProgram.setAttributeBuffer("vertexZ", GL_FLOAT, 0, 1);
    m_plotShaderProgram.enableAttributeArray("vertexZ");
    m_wrappedFunction.release();

    if (polygonOffset) {
        /*
         * void glPolygonOffset( GLfloat factor,
         *                       GLfloat units );
         *
         * factor:
         *      Specifies a scale factor that is used to create
         *      a variable depth offset for each polygon. The initial value is 0.
         *
         * units:
         *      Is multiplied by an implementation-specific value
         *      to create a constant depth offset. The initial value is 0.
         *
         * The value of the offset is factor×DZ+r×units, where DZ is a measurement of the change
         * in depth relative to the screen area of the polygon, and r is the smallest value
         * that is guaranteed to produce a resolvable offset for a given implementation.
         * The offset is added before the depth test is performed and before the value
         * is written into the depth buffer.
         *
         * ------------------------------------------------------------------------------------
         * From http://www.glprogramming.com/red/chapter06.html#name4
         * via https://stackoverflow.com/q/13431174/46058
         *
         * To achieve a nice rendering of the highlighted solid object without visual artifacts,
         * you can either add a positive offset to the solid object (push it away from you)
         * or a negative offset to the wireframe (pull it towards you).
         *
         * Small, non-zero values for factor, such as 0.75 or 1.0, are probably enough
         * to generate distinct depth values and eliminate the unpleasant visual artifacts.
         */
        glPolygonOffset(1, 0);
        glEnable(GL_POLYGON_OFFSET_FILL);
    }
    // draw
    // - surface
    if (checkDrawSurface) {
        // draw surface a little darker
        m_plotShaderProgram.setUniformValue("ambientColor",  QColor(180, 180, 180));
        m_surfaceMesh->drawElements();
    }
    // - grid / lines
    if (checkDrawLines) {
        // draw lines with default brightness
        m_plotShaderProgram.setUniformValue("ambientColor",  QColor(255, 255, 255));
        m_surfaceGrid->drawElements();
    }
    // - point cloud
    if (checkDrawPoints) {
        // draw points with default brightness
        m_plotShaderProgram.setUniformValue("ambientColor",  QColor(255, 255, 255));
        glDrawArrays(GL_POINTS, 0, static_cast<GLsizei>(m_plottedFunction.size()));
    }
    glPolygonOffset(0, 0);
    glDisable(GL_POLYGON_OFFSET_FILL);

    // bounding cube
    if (checkDrawCube) {
        drawBoundingCube(mvpMatrix, QColor(255,  16,  16), cubeLineWidth);
    }

    // release
    m_plotShaderProgram.disableAttributeArray("vertexXY");
    m_plotShaderProgram.disableAttributeArray("vertexZ");

    m_plotShaderProgram.release();
}

// based on draw_cube() function in 'src/graph.cpp' in
// https://gitlab.com/programowanie-gpu/func2d-glfw
void SurfacePlotGL::drawBoundingCube(QMatrix4x4 mvpMatrix,
                                     QColor lineColor, float lineWidth)
{
    GLfloat cube_vertices[][3] = {
        // back face
        {-1.0, -1.0, -1.0},
        { 1.0, -1.0, -1.0},
        { 1.0,  1.0, -1.0},
        {-1.0,  1.0, -1.0},
        // front face
        {-1.0, -1.0,  1.0},
        { 1.0, -1.0,  1.0},
        { 1.0,  1.0,  1.0},
        {-1.0,  1.0,  1.0},
    };
    GLfloat cube_colors[][3] = {
        // back face
        {1.0f, 1.0f, 0.1f}, // special case for (-1,-1,-1)
        {1.0f, 0.1f, 0.1f}, // lighter red
        {1.0f, 0.1f, 0.1f},
        {1.0f, 0.1f, 0.1f},
        // front face
        {1.0f, 0.1f, 0.1f},
        {1.0f, 0.1f, 0.1f},
        {1.0f, 0.1f, 1.0f}, // special case for ( 1, 1, 1)
        {1.0f, 0.1f, 0.1f},
    };
    GLushort cube_lines_indices[][2] = {
        // back face
        {0, 1},
        {1, 2},
        {2, 3},
        {3, 0},
        // front face
        {4, 5},
        {5, 6},
        {6, 7},
        {7, 4},
        // connect
        {0, 4},
        {1, 5},
        {2, 6},
        {3, 7},
    };

    // select shader program for drawing cube
    m_coloringShaderProgram.bind();

    // set values of uniform parameters
    // - transformation
    m_coloringShaderProgram.setUniformValue("mvpMatrix", mvpMatrix);

    // set values of per-vertex attribute arrays (input)
    // TODO: use VBO instead of copying data from CPU on each draw
    // - positions
    m_coloringShaderProgram.setAttributeArray("vertex", &(cube_vertices[0][0]), 3);
    m_coloringShaderProgram.enableAttributeArray("vertex");
    // - colors
    // TODO: pass color as uniform
    m_coloringShaderProgram.setAttributeArray("color", &(cube_colors[0][0]), 3);
    m_coloringShaderProgram.enableAttributeArray("color");

    // set line width
    glPushAttrib(GL_LINE_BIT);
    glLineWidth(lineWidth);

    // draw cube
    glDrawElements(GL_LINES, 12*sizeof(GLushort), GL_UNSIGNED_SHORT,
                   &(cube_lines_indices[0][0]));

    // reset line width
    glPopAttrib();

    // release shader program
    m_coloringShaderProgram.release();
}

void SurfacePlotGL::cleanup()
{
    qDebug("[%s] start", __PRETTY_FUNCTION__);

    if (!m_vertexXY.isCreated() || m_vertexXY.bufferId() == 0) {
        qDebug("[%s] end (EARLY)", __PRETTY_FUNCTION__);
        return;
    }
    m_wrappedFunction.destroy();
    m_vertexXY.destroy();
    m_surfaceMesh->destroy();
    qDebug("  vertexXY after destruction [%d]", m_vertexXY.bufferId());

    qDebug("[%s] end", __PRETTY_FUNCTION__);
}
