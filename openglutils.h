#ifndef OPENGLUTILS_H
#define OPENGLUTILS_H

#include <QtOpenGL>

#ifndef NDEBUG
#define check_gl_errors() _check_gl_errors(__FILE__,__LINE__)
#else
#define check_gl_errors() ((void)0)
#endif

/*
 * Alternative solutions:
 * - https://doc.qt.io/qt-5/qopengldebuglogger.html#details
 * - https://www.khronos.org/opengl/wiki/OpenGL_Error#Catching_errors_.28the_easy_way.29
 */
void _check_gl_errors(const char *file, unsigned int line);

#endif // OPENGLUTILS_H
