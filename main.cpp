#include <QApplication>
#include <QDesktopWidget>

#include "window.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

#ifndef NDEBUG
    QSurfaceFormat format;
    // asks for a OpenGL 3.2 debug context using the Core profile
    //format.setMajorVersion(3);
    //format.setMinorVersion(2);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setOption(QSurfaceFormat::DebugContext);

    QSurfaceFormat::setDefaultFormat(format);
    // alternative to setDefaultFormat:
    //QOpenGLContext *context = new QOpenGLContext;
    //context->setFormat(format);
    //context->create();
#endif /* !defined(NDEBUG) */

    Window window;

    window.setWindowTitle("Plot3D z=f(x,y) [OpenGL with Qt]");
    window.show();

    /*
    window.resize(window.sizeHint());

    int desktopArea = QApplication::desktop()->width() *
                      QApplication::desktop()->height();
    int widgetArea = window.width() *
                     window.height();

    if ((static_cast<float>(widgetArea) / static_cast<float>(desktopArea)) < 0.75f)
        window.show();
    else
        window.showMaximized();
    */

    return app.exec();
}
