#ifndef SURFACEPLOTGL_H
#define SURFACEPLOTGL_H

#include <memory>

#include <QVector3D>
#include <QMatrix4x4>
#include <QMatrix3x3>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

#include "funcxy.h"
#include "funcxygl.h"
#include "mesh2dgl.h"

class SurfacePlotGL : protected QOpenGLFunctions
{
public:
    SurfacePlotGL(const FuncXY &plottedFunction)
        : m_plottedFunction{plottedFunction},
          m_wrappedFunction{m_plottedFunction},
          m_surfaceMesh{new Surface::MeshTriangles(plottedFunction.grid())},
          m_surfaceGrid{new Surface::GridLines(plottedFunction.grid())}
    {}
    SurfacePlotGL(const FuncXY &plottedFunction,
                  std::unique_ptr<Surface::Mesh> surfaceMesh,
                  std::unique_ptr<Surface::Grid> surfaceGrid)
        : m_plottedFunction{plottedFunction},
          m_wrappedFunction{m_plottedFunction},
          m_surfaceMesh{std::move(surfaceMesh)},
          m_surfaceGrid{std::move(surfaceGrid)}
    {}
    SurfacePlotGL(float xmin, float xmax,
                  float ymin, float ymax,
                  GLuint Nx, GLuint Ny,
                  func2d_t func);

    // named after method names in QOpenGLWidget
    void initializeGL();
    void draw(QMatrix4x4 modelTransformationMatrix,
              QMatrix4x4 viewTransformationMatrix,
              QMatrix4x4 perspectiveProjectionMatrix,
              QVector3D spotlightPosition);
    void drawBoundingCube(QMatrix4x4 mvpMatrix,
                          QColor lineColor, float lineWidth = 1.0);
    void cleanup();

    // turn on and off drawing surface, wireframe, points
    // TODO: make it private, use getters and setters
    bool checkDrawSurface = true;
    bool checkDrawLines   = true;
    bool checkDrawPoints  = true;
    // turn on and off polygon offset (see glPolygonOffset)
    bool polygonOffset = true;
    // turn on and off bounding cube (or rather unit cube)
    // and set its parameters
    bool checkDrawCube = true;
    float cubeLineWidth = 2.0f;

private:
    FuncXY m_plottedFunction;
    BufferWrapper::FuncXY<FuncXY> m_wrappedFunction;

    std::unique_ptr<Surface::Mesh> m_surfaceMesh; ///< IBO for drawing surface of a plot
    std::unique_ptr<Surface::Grid> m_surfaceGrid; ///< IBO for drawing grid lines

    // OpenGL drawing
    QOpenGLShaderProgram m_plotShaderProgram; // for z = f(x,y), i.e. surface, lines, points
    QOpenGLShaderProgram m_coloringShaderProgram; // simple shaders with per-vertex color
    QOpenGLBuffer m_vertexXY{QOpenGLBuffer::VertexBuffer};

    // OpenGL extra transformation
    QVector3D m_scaling;
};

#endif // SURFACEPLOTGL_H
